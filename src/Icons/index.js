
import { FiUser } from "react-icons/fi";
import { FiShoppingCart } from "react-icons/fi";
import { AiFillAlipayCircle } from "react-icons/ai";
import { BsCart3 } from "react-icons/bs";
export const Icons = {
  FiUser,
  FiShoppingCart,
  AiFillAlipayCircle,
  BsCart3,
}



