export const color = {
    textColorBlack: "#000000",
    // border
    borderColorSmall: '#dee2e6',
    borderColorMenuIcon: '#cccccc',
    borderColorHeaderTitle: '#f1f1f1',
}
export const shadow = {
    IconMenu: '0 0 12px rgb(0 0 0 / 10%)'
}