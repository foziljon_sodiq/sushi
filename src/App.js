import "./App.css";
import { BrowserRouter, } from "react-router-dom";
import UseContextComponent from "./Context";
import HomePageComponent from "./pages/Home";

import Footer from "./Components/Footer";
import Contact from "./pages/Contact";
import ProductModal from "./Components/ProductModal";
import Register from "./pages/Register";
import { Modal } from "@mui/material";
import BasicModal from "./Components/Mod/mod";

function App() {
  function foo() {
    return true;
  }
  return (
    <>
      <UseContextComponent>
        <BrowserRouter>
          <HomePageComponent />
          <Container>
            <Product />
            <ProductModal />
            <Register />
          </Container>

          <BasicModal />
          <Footer />
        </BrowserRouter>
      </UseContextComponent>
    </>
  );
}

export default App;
