import React from 'react'
import HomeHeaderTitleComponent from '../../Components/Home/HomeHeaderTitle'
import HomeMenuComponent from '../../Components/Home/HomeMenu'
import Product from "../../Components/Product";
import { Container } from "@mui/system";
import CarouselComponent from '../../Components/Carousel';
import TitleComponent from '../../Components/Typography/Title';
import CardFoodBoxContainer from '../../Components/Card/CardFoodBox';
function HomePageComponent() {
    const props = {
        title: {
            text: 'Меню'
        }
    }
    const { title } = props
    return (
        <>
            <HomeHeaderTitleComponent />
            <Container>
                <HomeMenuComponent />
                <CarouselComponent />
                <TitleComponent objectProps={title} />
                <CardFoodBoxContainer />
                <Product />
            </Container>
        </>
    )
}

export default HomePageComponent