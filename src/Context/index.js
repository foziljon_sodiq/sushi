import React, { createContext } from 'react'
export const UseContext = createContext()
function UseContextComponent({ children }) {
    const values = {
    }
    return (
        <UseContext.Provider value={values}>
            {children}
        </UseContext.Provider>
    )
}

export default UseContextComponent