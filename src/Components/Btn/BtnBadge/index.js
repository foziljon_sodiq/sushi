import React from 'react'
import { ButtonBase } from '@mui/material'
import { StyledBadge, useStyles } from "./styles"
function BtnBadgeComponent({ objectProps }) {
    const { Icon, fun } = objectProps
    const classes = useStyles()
    return (
        <ButtonBase className={classes.btn} onClick={fun}>
            <StyledBadge badgeContent={3} color="secondary">
                <Icon className={classes.icon} />
            </StyledBadge>
        </ButtonBase>
    )
}

export default BtnBadgeComponent