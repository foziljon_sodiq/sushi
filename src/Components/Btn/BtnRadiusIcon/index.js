import { ButtonBase } from '@mui/material'
import React from 'react'
import { useStyles } from './styles'

function BtnRadiusIconComponent({ objectProps }) {
    const { Icon } = objectProps
    const classes = useStyles()
    return (
        <ButtonBase className={classes.btn}>
            <Icon className={classes.icon} />
        </ButtonBase>
    )
}

export default BtnRadiusIconComponent