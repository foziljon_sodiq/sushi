import { makeStyles } from "@mui/styles"
import { shadow } from "../../../Styles/Colors"
const { IconMenu } = shadow
export const useStyles = makeStyles({
    btn: {
        width: '46px',
        height: '46px',
        boxShadow: IconMenu,
        margin: '0 1rem 0 0 !important',
        borderRadius: '50% !important',
    },
    icon: {
        'fontSize': '18px'
    },
})