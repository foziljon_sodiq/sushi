import * as React from 'react';
import { useTheme } from '@mui/material/styles';
import Box from '@mui/material/Box';
import { images } from '../../images';

import SwipeableViews from 'react-swipeable-views';
import { autoPlay } from 'react-swipeable-views-utils';
const { HeaderSliderFistFood } = images
const AutoPlaySwipeableViews = autoPlay(SwipeableViews);

const image = [
    {
        imgPath: HeaderSliderFistFood,
        label: 'San Francisco – Oakland Bay Bridge, United States',
    },
    {
        label: 'Bird',
        imgPath: HeaderSliderFistFood,
    },
    {
        label: 'Bali, Indonesia',
        imgPath: HeaderSliderFistFood,
    },
    {
        label: 'Goč, Serbia',
        imgPath: HeaderSliderFistFood,
    },
];

function CarouselComponent() {
    const theme = useTheme();
    const [activeStep, setActiveStep] = React.useState(0);

    const handleStepChange = (step) => {
        setActiveStep(step);
    };

    return (
        <Box sx={{ maxWidth: '100%', flexGrow: 1 }}>
            <AutoPlaySwipeableViews
                index={activeStep}
                onChangeIndex={handleStepChange}
                enableMouseEvents
            >
                {image.map((step, index) => (
                    <div key={index}>
                        {Math.abs(activeStep - index) <= 2 ? (
                            <Box
                                component="img"
                                sx={{
                                    display: 'block',
                                    maxWidth: '100%',
                                    overflow: 'hidden',
                                    width: '100%',
                                }}
                                src={step.imgPath}
                                alt={step.label}
                            />
                        ) : null}
                    </div>
                ))}
            </AutoPlaySwipeableViews>
        </Box>
    );
}

export default CarouselComponent;