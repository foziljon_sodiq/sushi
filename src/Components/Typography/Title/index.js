import { Box } from '@mui/material'
import React from 'react'
import { useStyles } from './styles'
function TitleComponent({ objectProps }) {
    const { text } = objectProps
    const classes = useStyles()
    return (
        <Box className={classes.root}>
            {text}
        </Box>
    )
}

export default TitleComponent