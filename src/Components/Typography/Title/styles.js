import { makeStyles } from "@mui/styles";
import { size } from "../../../Styles/Size";
const { titleSize } = size
export const useStyles = makeStyles({
    root: {
        fontSize: titleSize,
        margin: '35px 0 0 0',
        fontWeight: 600
    }
})