import { Box } from '@mui/material'
import React from 'react'
import { useStyles } from './styles'

function TextCardMenuComponent({ objectProps }) {
  const classes = useStyles()
  const { text } = objectProps

  return (
    <Box>
      {text}
    </Box>
  )
}

export default TextCardMenuComponent