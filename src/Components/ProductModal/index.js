import * as React from "react";
import {
  Box,
  Button,
  Modal,
  Card,
  CardContent,
  CardMedia,
  IconButton,
  Typography,
} from "@mui/material";
import { images } from "../../images";
import { useTheme } from "@mui/material/styles";
import { useStyles } from "./style";

export default function ProductModal() {
  const style = useStyles();
  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);
  const { CardImages } = images;
  const theme = useTheme();

  // const style = {
  //   position: "absolute",
  //   top: "50%",
  //   left: "50%",
  //   transform: "translate(-50%, -50%)",
  //   width: 700,
  //   bgcolor: "background.paper",
  //   borderRadius: 5,
  //   boxShadow: 24,
  //   p: 4,
  //   display: "flex",
  // };

  return (
    <div>
      {console.log(style)}
      <Button onClick={handleOpen} sx={{}}>
        Open modal
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box className={style.boxWrap}>
          <CardMedia
            component="img"
            sx={{ width: 350 }}
            image={CardImages}
            alt="Live from space album cover"
          />
          <Box sx={{ display: "flex", flexDirection: "column", marginLeft: 4 }}>
            <CardContent sx={{ flex: "1 0 auto" }}>
              <Typography component="div" variant="h5">
                Live From Space
              </Typography>
              <Typography
                variant="subtitle1"
                color="text.secondary"
                component="div"
              >
                Mac Miller
              </Typography>
            </CardContent>
            <Box sx={{ display: "flex", alignItems: "center", pl: 1, pb: 1 }}>
              <IconButton aria-label="delete" size="large">
                +
              </IconButton>
              <Typography component="span" variant="h4">
                0
              </Typography>
              <IconButton aria-label="delete" size="large">
                -
              </IconButton>
            </Box>
          </Box>
        </Box>
      </Modal>
    </div>
  );
}
