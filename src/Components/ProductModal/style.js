import { makeStyles } from "@mui/styles";
export const useStyles = makeStyles({
  boxWrap: {
    background: "white!important",
    position: "absolute!important",
    top: "50%!important",
    left: "50%!important",
    transform: "translate(-50%, -50% )!important",
    width: 700,
    bgColor: "background.paper!important",
    borderRadius: 5,
    boxShadow: 24,
    p: 4,
    display: "flex!important",
  },
  btnModal: {
    backgroundColor: "black!important",
    color: "white!important",
  },
});
