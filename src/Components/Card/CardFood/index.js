import React from 'react'
import { Box } from '@mui/material'
import { useStyles } from './styles'
import TextCardMenuComponent from '../../Typography/Text'
import { images } from '../../../images'
const { GumbergerBlack } = images
function CardFoodComponent() {
    const classes = useStyles()
    const propsObject = {
        textObject: {
            text: 'Бургеры'
        }
    }
    const { text } = propsObject
    return (
        <Box className={classes.root}>
            <Box className={classes.card}>
                <img src={GumbergerBlack} alt="card Image" />
            </Box>
            <TextCardMenuComponent objectProps={text} />
        </Box>
    )
}

export default CardFoodComponent