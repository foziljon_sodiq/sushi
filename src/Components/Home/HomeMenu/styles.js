
import { makeStyles } from "@mui/styles"
import { width } from "../../../Styles/Size"

const { logoHeight } = width
export const useStyles = makeStyles({
    root: {
        'display': 'flex',
        alignItems: 'center',
        padding: '1rem',
    },
    img: {
        height: logoHeight,
    },
    menuContainer: {
        'display': 'flex',
        'justifyContent': 'space-between',
        width: '100%',
        padding: '0 1rem'
    },
    iconContainer: {
        display: 'flex',
    },
    badge: {
        borderRadius: '50%',
    }
})
