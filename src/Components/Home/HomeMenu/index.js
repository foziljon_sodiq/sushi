import React from 'react'
import { Box } from '@mui/system'
import { useStyles } from './styles'
import { Icons } from '../../../Icons'
import { images } from '../../../images'
import { ButtonBase } from '@mui/material'
import BtnBadgeComponent from '../../Btn/BtnBadge'
import BtnRadiusIconComponent from '../../Btn/BtnRadiusIcon'

function HomeMenuComponent() {
    const { logo } = images
    const { FiUser, FiShoppingCart } = Icons
    const classes = useStyles()
    const menuArr = [
        {
            id: 1,
            text: "One",
            data: []
        },
        {
            id: 1,
            text: "Two",
            data: []
        },
        {
            id: 1,
            text: "Three",
            data: []
        },
        {
            id: 1,
            text: "Four",
            data: []
        },
    ]
    const props = {
        btnBadge: {
            Icon: FiShoppingCart,
            fun: () => {
                console.log("FiShoppingCart")
            }
        },
        btnRadius: {
            Icon: FiUser,
            fun: () => {
                console.log('FiUser')
            }
        }
    }
    const { btnBadge, btnRadius } = props
    return (
        <>
            <Box className={classes.root}>
                <img className={classes.img} src={logo} alt="logo" />
                <Box className={classes.menuContainer}>
                    {menuArr.map((i, idx) =>
                        <ButtonBase key={idx} className={classes.menuItem}>
                            {i.text}
                        </ButtonBase>
                    )}
                </Box>
                <Box className={classes.iconContainer}>
                    <BtnRadiusIconComponent objectProps={btnRadius} />
                    <BtnBadgeComponent objectProps={btnBadge} />
                </Box>
            </Box>
        </>
    )
}

export default HomeMenuComponent