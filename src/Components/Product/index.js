import { Grid, Tooltip } from "@mui/material";
import React from "react";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Typography from "@mui/material/Typography";
import { Button, CardActionArea, CardActions, Box } from "@mui/material";
import { images } from "../../images";
import Zoom from "@mui/material/Zoom";
import { grey } from "@mui/material/colors";
import { useStyles } from "./style";
import { Icons } from "../../Icons";

export default function Product({}) {
  const { CardImages } = images;
  const greyTitle = grey[600]; // #f44336

  const style = useStyles();

  return (
    <>
      <Grid container spacing={3} sx={{ my: 4 }}>
        {/* <Grid item sm={1} display={{ sm: "none", xs: "block" }}>
          3
        </Grid> */}
        <Grid item md={3} sm={6} xs={12}>
          <Card sx={{ maxWidth: 345, borderRadius: 5 }}>
            <>
              <CardMedia
                component="img"
                height="250"
                image={CardImages}
                alt="green iguana"
              />
              <CardContent>
                <Typography gutterBottom variant="h5" component="div">
                  Avakado Hosamaki
                </Typography>
                <Typography
                  gutterBottom
                  variant="p"
                  component="div"
                  color={greyTitle}
                >
                  Авокадо
                </Typography>
                <CardActions sx={{ p: 0, mt: 3 }}>
                  <Tooltip
                    title=" 8шт "
                    placement="top"
                    arrow
                    TransitionComponent={Zoom}
                  >
                    <Button size="small" className={style.buttonWhite}>
                      8шт
                    </Button>
                  </Tooltip>
                  <Tooltip
                    title=" 16шт "
                    placement="top"
                    arrow
                    TransitionComponent={Zoom}
                  >
                    <Button size="small" className={style.buttonBlack}>
                      16шт
                    </Button>
                  </Tooltip>
                </CardActions>
                <CardActions className={style.priceWrap} sx={{ p: 0, mt: 3 }}>
                  <Typography
                    gutterBottom
                    variant="b"
                    component="div"
                    className={style.price}
                  >
                    3.50
                    <span> € </span>
                  </Typography>
                  <Box className={style.basket} variant="div" component="div">
                    <Icons.BsCart3 />
                  </Box>
                </CardActions>
              </CardContent>
            </>
          </Card>
        </Grid>
      </Grid>
    </>
  );
}
