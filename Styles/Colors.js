export const color = {
  textColorBlack: '#000000',
  borderColorSmall: '#dee2e6',
  borderColorHeaderTitle: '#f1f1f1',
}
