export const size = {
  size16: '16px',
  size14: '14px',
}
export const width = {
  logoWidth: '200px',
  logoHeight: '54px',
}
