import logo from './logo.webp'
import IconTitle from './IconTitle.webp'
import CardImages from './CardImages.webp'
import GumbergerBlack from './GumbergerBlack.webp'
import HeaderSliderFistFood from './HeaderSliderFistFood.webp'
export const images = {
    logo,
    IconTitle,
    CardImages,
    GumbergerBlack,
    HeaderSliderFistFood
}