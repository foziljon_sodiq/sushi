import * as React from 'react'
import {
  Box,
  Modal,
  CardContent,
  CardMedia,
  IconButton,
  Typography,
} from '@mui/material'

import { useStyles } from './style'

export default function ProductModal({ open, setOpen, selectProduct }) {
  const style = useStyles()
  const handleClose = () => setOpen(false)

  return (
    <div>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby='modal-modal-title'
        aria-describedby='modal-modal-description'
      >
        <Box className={style.boxWrap}>
          <CardMedia
            component='img'
            sx={{ width: 100 }}
            image={selectProduct?.image}
            alt='Live from space album cover'
            className={style.image}
          />
          <Box sx={{ display: 'flex', flexDirection: 'column', marginLeft: 4 }}>
            <CardContent sx={{ flex: '1 0 auto' }}>
              <Typography component='div' variant='h5'>
                {selectProduct?.title}
              </Typography>
              <Typography
                variant='subtitle1'
                color='text.secondary'
                component='div'
              >
                {selectProduct?.category}
              </Typography>
            </CardContent>
            <Box sx={{ display: 'flex', alignItems: 'center', pl: 1, pb: 1 }}>
              <IconButton aria-label='delete' size='large'>
                +
              </IconButton>
              <Typography component='span' variant='h4'>
                0
              </Typography>
              <IconButton aria-label='delete' size='large'>
                -
              </IconButton>
            </Box>
          </Box>
        </Box>
      </Modal>
    </div>
  )
}
