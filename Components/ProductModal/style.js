import { makeStyles } from '@mui/styles'
export const useStyles = makeStyles({
  boxWrap: {
    background: 'white!important',
    position: 'absolute!important',
    top: '50%!important',
    left: '50%!important',
    transform: 'translate(-50%, -50% )!important',
    width: 700,
    bgColor: 'background.paper!important',
    borderRadius: 15,
    boxShadow: 24,
    p: 4,
    display: 'flex !important',
    border: 'none',
    outline: 'none',
    padding: '15px',
    transition: 'all 1s linear',
  },

  btnModal: {
    backgroundColor: 'black!important',
    color: 'white!important',
  },
  image: {
    width: '350px !important',
    height: '350px !important',
    objectFit: 'cover !important',
  },
})
