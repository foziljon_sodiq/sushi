import { makeStyles } from '@mui/styles'
export const useStyles = makeStyles({
  footerWrap: {
    width: '100%',
    padding: '36px 0',
    backgroundColor: '#373F50',
  },
  footerTitle: {
    fontWeight: 'bold',
    color: 'white',
  },
  footerText: {
    fontWeight: 'normal',
    color: '#B9BCC2',
  },

  buttonWhite: {
    backgroundColor: 'black!important',
    color: 'white!important',
    padding: '3px 8px!important',
    borderRadius: '20px!important',
    transition: '0.3s!important',
    border: '1px solid transparent!important',
    marginRight: '16px!important',
  },
  buttonBlack: {
    backgroundColor: 'white!important',
    color: 'black!important',
    padding: '3px 8px!important',
    borderRadius: '20px!important',
    transition: '0.3s!important',
    border: '1px solid black!important',
  },
  priceWrap: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  price: {
    fontSize: '18px',
    fontWeight: 'bold',
    marginBottom: '0!important',
    cursor: 'auto',
  },
  basket: {
    width: '44px',
    height: '44px',
    borderRadius: '50%',
    backgroundColor: 'black',
    display: 'inline-block',
    fontSize: '18px',
    color: 'white',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    transition: '0.3s',
    '&:hover': {
      backgroundColor: 'red',
      color: 'white',
      cursor: 'pointer',
    },
  },
  root: {
    display: 'flex',
  },

  menuContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%',
  },
})
