import { Grid, Tooltip } from '@mui/material'
import React from 'react'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import Typography from '@mui/material/Typography'
import { Button, CardActionArea, CardActions, Box, Link } from '@mui/material'
import { images } from '../../images'
import Zoom from '@mui/material/Zoom'
import { grey } from '@mui/material/colors'
import { useStyles } from './style'
import { Icons } from '../../Icons'
import { Container } from '@mui/system'

export default function Footer() {
  const { CardImages } = images
  const greyTitle = grey[600]
  const style = useStyles()

  return (
    <Box className={style.footerWrap}>
      <Container>
        <Grid container spacing={3} sx={{ my: 4 }}>
          <Grid item md={4} sm={6} xs={12} className={style.footerTitle}>
            <Typography sx={{ mb: 5 }} variant='h5' component='div'>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
          </Grid>
          <Grid item md={5} sm={6} xs={12} className={style.footerTitle}>
            <Typography sx={{ mb: 5 }} variant='h5' component='div'>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
          </Grid>
          <Grid item md={3} sm={6} xs={12} className={style.footerTitle}>
            <Typography sx={{ mb: 5 }} variant='h5' component='div'>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
            <Typography component='p' className={style.footerText}>
              Avakado Hosamaki
            </Typography>
          </Grid>
        </Grid>
      </Container>
    </Box>
  )
}
