import { Box } from '@mui/material'
import React from 'react'
import { useStyles } from './styles'

function HomeHeaderTitleComponent() {
  const classes = useStyles()
  return (
    <Box className={classes.root}>
      <Box className={classes.phone}>+371 24665544</Box>
      <Box className={classes.border} />
      <Box className={classes.title}>
        SV-C:NO 14:00 LĪDZ 03:00 | PK-S:NO 14:00 LĪDZ 05:00
      </Box>
    </Box>
  )
}

export default HomeHeaderTitleComponent
