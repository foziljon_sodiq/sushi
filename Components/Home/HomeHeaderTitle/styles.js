import { makeStyles } from "@mui/styles"
import { color } from "../../../Styles/Colors"
import { size } from "../../../Styles/Size"
const { borderColorHeaderTitle, borderColorSmall } = color
const { size14 } = size
export const useStyles = makeStyles({
    root: {
        'display': 'flex',
        'padding': "1rem",
        "alignItems": 'center',
        'border-bottom': `1px solid ${borderColorHeaderTitle}`
    },
    title: {
        fontSize: size14
    },
    phone: {

    },
    border: {
        'width': '2px',
        "height": '21px',
        'backgroundColor': borderColorSmall,
        'margin': "0 1rem"
    }
})