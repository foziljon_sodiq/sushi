import { makeStyles } from '@mui/styles'
import { width } from '../../../Styles/Size'
const { logoHeight } = width
export const useStyles = makeStyles({
  root: {
    display: 'flex',
  },
  img: {
    height: logoHeight,
  },
  menuContainer: {
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%',
  },
})
