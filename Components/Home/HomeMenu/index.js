import { Modal } from '@mui/material'
import { Box } from '@mui/system'
import React, { useState } from 'react'
import { images } from '../../../images'
import { useStyles } from './styles'

function HomeMenuComponent() {
  const { logo } = images

  const classes = useStyles()
  const menuArr = [
    {
      id: 1,
      text: 'first',
      data: [],
    },
    {
      id: 1,
      text: 'Two',
      data: [],
    },
    {
      id: 1,
      text: 'Three',
      data: [],
    },
    {
      id: 1,
      text: 'Four',
      data: [],
    },
  ]
  return (
    <>
      <Box className={classes.root}>
        <img className={classes.img} src={logo} alt='logo' />
        <Box className={classes.menuContainer}>
          {menuArr.map((i, idx) => (
            <Box
              key={idx}
              className={classes.menuItem}
              onClick={i.click ? i.click : () => {}}
            >
              {i.text}
            </Box>
          ))}
        </Box>
        <Box className={classes.iconContainer}></Box>
      </Box>
    </>
  )
}

export default HomeMenuComponent
