import { Grid, Tooltip } from '@mui/material'
import React, { useState } from 'react'
import Card from '@mui/material/Card'
import CardContent from '@mui/material/CardContent'
import CardMedia from '@mui/material/CardMedia'
import Typography from '@mui/material/Typography'
import { Button, CardActions, Box } from '@mui/material'
import Zoom from '@mui/material/Zoom'
import { grey } from '@mui/material/colors'
import { useStyles } from './style'
import { Icons } from '../../Icons'

export default function Product({
  product,
  addFavoriteProduct,
  setOpenModal,
  setSelectProduct,
}) {
  const greyTitle = grey[600] // #f44336
  const [twoProduct, setTwoProduct] = useState(false)
  const style = useStyles()
  const changeProduct = () => {
    setOpenModal(true)
    setSelectProduct(product)
  }
  return (
    <>
      <Grid item md={3} sm={6} xs={12}>
        <Card sx={{ maxWidth: 345, borderRadius: 5 }}>
          <>
            <CardMedia
              component='img'
              height='250'
              image={product.image}
              alt='green iguana'
              onClick={changeProduct}
              className={style.image}
            />
            <CardContent>
              <Typography gutterBottom variant='h5' component='div'>
                {product.title
                  ? product.title.slice(0, 20)
                  : 'Sotib olib kuymaysan'}
              </Typography>
              <Typography
                gutterBottom
                variant='p'
                component='div'
                color={greyTitle}
              >
                {product.category}
              </Typography>
              <CardActions sx={{ p: 0, mt: 3 }}>
                <Tooltip
                  title=' 8шт '
                  placement='top'
                  arrow
                  TransitionComponent={Zoom}
                >
                  <Button
                    size='small'
                    className={
                      twoProduct ? style.buttonBlack : style.buttonWhite
                    }
                    onClick={() => setTwoProduct(false)}
                  >
                    8шт
                  </Button>
                </Tooltip>
                <Tooltip
                  title=' 16шт '
                  placement='top'
                  arrow
                  TransitionComponent={Zoom}
                >
                  <Button
                    size='small'
                    className={
                      !twoProduct ? style.buttonBlack : style.buttonWhite
                    }
                    onClick={() => setTwoProduct(true)}
                  >
                    16шт
                  </Button>
                </Tooltip>
              </CardActions>
              <CardActions className={style.priceWrap} sx={{ p: 0, mt: 3 }}>
                <Typography
                  gutterBottom
                  variant='b'
                  component='div'
                  className={style.price}
                >
                  {twoProduct ? product.price * 2 : product.price}
                  <span> € </span>
                </Typography>
                <Box className={style.basket} variant='div' component='div'>
                  <Icons.BsCart3 onClick={() => addFavoriteProduct(product)} />
                </Box>
              </CardActions>
            </CardContent>
          </>
        </Card>
      </Grid>
    </>
  )
}
