import { AiFillAlipayCircle } from "react-icons/ai";
import { BsCart3 } from "react-icons/bs";

export const Icons = {
  AiFillAlipayCircle,
  BsCart3,
};
