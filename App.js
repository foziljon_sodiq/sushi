/* eslint-disable react-hooks/exhaustive-deps */
import './App.css'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import UseContextComponent from './Context'
import HomePageComponent from './pages/Home'
import Product from './Components/Product'
import { Container } from '@mui/system'
import Footer from './Components/Footer'
import Contact from './pages/Contact'
import ProductModal from './Components/ProductModal'
import { useEffect, useState } from 'react'
import instance from './axios/axios'
import { Grid, Modal } from '@mui/material'
function App() {
  const [products, setProducts] = useState([])
  const [favoriteProducts, setFavoriteProducts] = useState([])
  const [selectProduct, setSelectProduct] = useState({})

  useEffect(() => {
    instance.get('/products').then((data) => setProducts(data.data))
  }, [])
  const [open, setOpen] = useState(false)

  const addFavoriteProduct = (item) => {
    setFavoriteProducts([...favoriteProducts, item])
  }
  return (
    <>
      <UseContextComponent>
        <BrowserRouter>
          <HomePageComponent />
          <Container>
            <Grid container spacing={3} sx={{ my: 4 }}>
              {products?.map((item) => (
                <Product
                  product={item}
                  key={item.id}
                  addFavoriteProduct={addFavoriteProduct}
                  setOpenModal={setOpen}
                  setSelectProduct={setSelectProduct}
                />
              ))}
            </Grid>
            <ProductModal
              open={open}
              setOpen={setOpen}
              selectProduct={selectProduct}
            />
          </Container>
          <Footer />
        </BrowserRouter>
      </UseContextComponent>
    </>
  )
}

export default App
