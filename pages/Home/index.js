import React from 'react'
import HomeHeaderTitleComponent from '../../Components/Home/HomeHeaderTitle'
import HomeMenuComponent from '../../Components/Home/HomeMenu'

function HomePageComponent({ favoriteProducts }) {
  return (
    <>
      <HomeHeaderTitleComponent />
      <HomeMenuComponent favoriteProducts={favoriteProducts} />
    </>
  )
}

export default HomePageComponent
